# evalhyd-r notebook

Repository hosting files to run a notebook for `evalhyd-r` on MyBinder:

[Launch a Jupyter notebook](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.irstea.fr%2FHYCAR-Hydro%2Fevalhyd%2Fevalhyd-notebooks%2Fr/HEAD?filepath=evalhyd-r.ipynb)
